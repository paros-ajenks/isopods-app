# Isopods (Experimental) - Disillusioned Music App

I was an avid proponent of Google Play Music. They killed it. This will be a superior replacement (one day). For now it's a home for all my experimental code ideas. Definitely not production-ready.

I am not good at naming projects, seldom few programmers are, so Isopods is the working title. There's a joke to be had somewhere.

## Design Choices

I chose React because this is a mobile-first application. SPAs are a good candidate, because when done correctly, they can feel just as smooth and performant as a native application.
Node.js for the backend for scalability and performance. If this were doing anything more complicated, I would opt for something like Java or C++.

For the desktop portion, I chose [Electron](https://www.electronjs.org/). For mobile, at the moment, [`capacitor.js`](https://capacitorjs.com/), however I may go for something else in the future depending on how the test implementation goes and if [some missing features can be implemented](https://github.com/ionic-team/capacitor/discussions/3969).

Currently I am experimenting with how you render pages (called "views" here). Contained in [`/src/views/index.js`](src/views/index.js) is the bootstrapping of three different experimental implementations, as well as a basic explanation of why these experiments are being done in the first place. None of it will be the final production-level code, really it's more to figure out what is possible, and how to optimize React UI/UX for the mobile world.

Additionally, this repo contains my experimental UI component library eponymously titled "neuomorphic". See [`/src/components/neuomorphic`](src/components/neuomorphic).

### JS

* Minimal transpilation - stick to the available spec in the wild where possible.
* Root-level imports using the `"~/..."` syntax where necessary to increase code readability
* Prefer exporting both explicits **and** defaults where possible
* Optimize imports for bundle size, code split where possible
* Platform-specific code is isolated using the [`deviceType`](src/util/deviceType.js) utilities to reduce bundle sizes
* Prefer ephemeral data - Utilize systems like `sessionStorage` over `localStorage` where necessary
* Separation of concerns - Spaghetti code happens sometimes, but as long as it only does one thing
* TypeScript definitions and JSDoc comments where necessary for Intellisense purposes

### React

* [`react-app-rewired`](https://github.com/timarney/react-app-rewired) is leveraged to customize ESLint/Babel without ejecting
* Function components with hooks where possible (for consistency)
* Separation of pages (called "views") and components.
* Dedicated view manager to handle page transitions and some state (see explanation below)
* Modular folders with assets. Most components should be atomic, isolated and reusable
* Loading skeletons and/or indicators for **any** long-form action. Maximize UX where possible
* Minimize global state - MobX is the state library of choice for this project
* Leverages a weird form system I built, subject to updates but API will not change. Will move it out of the private git/NPM server eventually

### CSS / General Design

* Loosely based on Google Play Music mixed with Neuomorphism (Dark) for default theme
* Leverage SASS over CSS for advanced style and improved readability
* [`bulma`](https://bulma.io/documentation/overview/modifiers/)-style modifier syntax for class names
* Consistent palette and component design with highlights for actions
* Scope styles to prevent overlap between components where possible
* Follow the [Material Design Guidelines](https://material.io/design) for accessibility and general layout

## Technical Challenges

### View Stacking (or a justification of a "PageService" implementation)

One of the largest issues with React in a mobile setting is the idea of view stacking. In a typical Android application, activities are pushed and popped from a stack, usually with nice, smooth transitions. Some activities slide up as overlays, most are full-size.

![GPM Screenshots](https://www.techadvisor.co.uk/cmsdata/slideshow/3673856/best-android-music-players-google-play_thumb800.jpg)

In the case of Google Play Music, there was a persistent bottom bar for the current queue. It was not visible if there was no current queue. Minimized, it showed the play/pause and basic track info. However, you could tap or drag it up to a full-size two-mode view. The first view was of the currently-playing track. At the top was the track info and a queue toggle button, with a full-size album art display. At the bottom was media controls and like/dislike buttons. Toggling the queue button showed the current play queue, allowed for play order adjustments and other queue tasks. I am not yet sure of a good way to implement this in React, and so the experiments in this repo are attempts to fill that void. The most promising one thus far is the [PageService](src/services/PageService.js), which allows for independent view and navigation rendering. It composes the two together, supports preloading, and more so far, but soon its primary role will be managing these "sub-views" like the aforementioned queue bar at the bottom.

The primary reason to utilize these as views and not as components is browser history / state. You should be able to directly link to most major functions in an application for UX purposes, and not being able to link to the queue is less than ideal. Additionally, this will be rendering in both an Electron context and a capacitor.js context, meaning it has to function smooth as butter across two very different webview implementations.

### Cross-Platform without React Native

Description coming soon.