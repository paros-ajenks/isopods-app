import merge from "lodash/merge";

let config = {
    version: "",
    commit: "",
    env: "",
    urls: {

    }
};

merge(config, {
    env: process.env["NODE_ENV"] || "development",
    version: process.env["REACT_APP_VERSION"],
    commit: process.env["REACT_APP_COMMIT_HASH"]
});

let meta = document.createElement("meta");
meta.name = "ReactAppVersion";
meta.content = config.version;

document.head.prepend(meta);

export default config;
