import { createBrowserHistory } from "history";
import { getHashString, getQueryString } from "~/util/address";

const history = createBrowserHistory({});

/**
 * Redirect to a page with hash/query preservation
 * @param {String | Object} url 
 * @param {{ hash: boolean, query: boolean }} preserve
 */
history.redirect = (url, { hash = false, query = false } = {}) => {
    if(query) url += `?${getQueryString()}`;
    if(hash) url += `#${getHashString()}`;

    return history.push(url);
};

export default history;