import { EventEmitter } from "events";

let pipeline = new EventEmitter();
pipeline.setMaxListeners(255);

let context = new AudioContext();
let audio = new Audio();
audio.crossOrigin = "anonymous";
audio.preload = "all";

let queue = [];
let index = -1;

audio.addEventListener("canplay", () => pipeline.emit("ready"));
audio.addEventListener("play", () => pipeline.emit("playstatus", true));
audio.addEventListener("pause", () => pipeline.emit("playstatus", false));
audio.addEventListener("waiting", () => pipeline.emit("buffering"));
audio.addEventListener("ended", () => pipeline.emit("done"));

export default new class MediaServices {
    once = pipeline.once.bind(pipeline);
    off = pipeline.off.bind(pipeline);
    on = pipeline.on.bind(pipeline);
    
    playAudio = (data, queue = true) => {
        if(queue) {
            
        } else {

        }
    }

    pause = () => {

    }

    play = () => {

    }

    setQueue = (items, autoplay = false) => {

    }
}