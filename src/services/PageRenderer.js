import React from "react";
import { observable, observe, toJS } from "mobx";
// import { prepLazy } from "~/util/lazy";
// import { Router, Route/*, Redirect, matchPath*/ } from "react-router-dom";
// import LocalStorage from "./LocalStorage";
import history from "./BrowserHistory";
// import Logger from "~/util/Logger";

// const logger = new Logger("PageRenderer");

const Fragment = ({ children }) => children === undefined ? null : children;

// let pageStack = observable.array([], { deep: false });
// let redirectMap = {};

// const render = (location, history) => {

// }

// const onChange = ({ location, history }) => {
//     if(location.action) {

//     }


// };

// const MobileHistory = observable({
//     state: {
//         id: "library",
//         state: {},
//         subviews: []
//     }
// });

// class PageRendererComponent extends React.PureComponent {
//     state = {
//         view: null,
//         history: [],
//         currentIndex: 0
//     };

//     render() {
//         let { nav, view } = this.state;
//         // let { location, history } = this.props;
//         // let { stack }

//         return (
//             <React.Fragment>

//             </React.Fragment>
//         );
//     }
// }

// const getDisplay = ({ location, history }) => {

// };

const timeline = observable({
    history: [{ ...history.location }],
    index: 0,

    get current() {
        return this.history[this.index];
    }
});

observe(timeline, "index", ({ newValue }) => {
    console.log(`TIMELINE INDEX CHANGED TO ${newValue}!`, toJS(timeline.current));
});

window.addEventListener("popstate", e => console.log(e));

// history.listen(wrapAction(({ action, location, ...other }) => {
//     if(action === "PUSH") {
//         console.log("PUSH", other);
//         timeline.history.splice(timeline.index + 1, Infinity);
//         timeline.history.push({ ...location });
//         timeline.index++;
//     } else if(action === "POP") {
//         console.log("POP", other);
//         timeline.index--;
//     } else if(action === "REPLACE") {
//         console.log("REPLACE", other);
//         timeline.history.splice(timeline.index, Infinity);
//         timeline.history.push({ ...location });
//     }
// }));

let counter = 0;
setInterval(() => history.push(`/${counter++}`, { counter }), 3000);

// const Router = ({ history }) => {
//     return <Fragment />;
// };

export default {
    buildPageRenderer: (Providers = Fragment) => (
        <Providers history={history}>
                {/* <Route render={getDisplay} /> */}
        </Providers>
    )
};
