import React from "react";
import { prepLazy } from "~/util/lazy";
import { Router, Route, Redirect, matchPath } from "react-router-dom";
import LocalStorage from "./LocalStorage";
import history from "./BrowserHistory";
// import Logger from "~/util/Logger";

// const logger = new Logger("PageService");

const Fragment = ({ children }) => children;

// document.addEventListener("click", e => {
//     history.push("/library", { nonce: +new Date() });
// });

/**
 * PageService is in charge of handling the browser state, i.e. where you are, where you were, and where you're going.
 * It can preload pages, compose views of layouts and view components, and more. Or at least, it will when it's done
 */
class PageService {
    redirects = {};

    registeredPaths = [];

    navigationTemplates = {};
    paths = {};
    pages = {};
    pageStack = [];

    constructor() {
        window.addEventListener("beforeunload", () => {
            LocalStorage.setItem("pageStack", this.pageStack);
        });

        let pageStack = LocalStorage.getItem("pageStack", []);
        if(Array.isArray(pageStack)) {
            this.pageStack = pageStack;
        }
    }

    addRedirect = (from, to) => this.redirects[from] = to;

    addNavigationTemplate = ({
        uid,
        component: Component,
        tags = [],
        alwaysPreload = false,
        preloadFallback = "Loading..."
    }) => {
        let { component, preload } = prepLazy(Component, preloadFallback);
        this.navigationTemplates[uid] = { component, preload, tags, alwaysPreload };

        if(alwaysPreload && component.preload) {
            component.preload();
        }
    }

    addPage = ({
        uid,
        navigationTemplate,
        component: Component,
        paths = [], // List of route paths
        subpages = [], // List of uids
        tags = [], // Used to preload other pages
        canPreload = true, // Used by other views to preload subpages
        alwaysPreload = false, // Used to preload initially
        preloadFallback = "Loading..."
    }) => {
        if(!Array.isArray(paths)) paths = [paths];
        let { component, preload } = prepLazy(Component, preloadFallback);

        let obj = { uid, navigationTemplate, paths, component, preload, subpages, tags, canPreload, alwaysPreload };
        this.pages[uid] = obj;
        for(let path of paths) {
            this.paths[path] = obj;
        }

        this.registeredPaths.push(...paths);

        if(alwaysPreload) {
            preload();
        }

        if(alwaysPreload && this.navigationTemplates[navigationTemplate]) {
            this.navigationTemplates[navigationTemplate].component.preload();
        }

        //! USED TO TEST
        history.push(paths[0], { test: "ASDF" });
    }

    preload = async(tag, uid) => {
        let tags = Array.isArray(tag) ? tag : [tag];
        let pages = Object.keys(this.paths).filter(x => this.paths[x].tags.find(y => tags.includes(y))).map(x => this.paths[x].preload);
        let subpages = this.pages[uid].subpages.map(x => this.pages[x].preload);
        let templates = pages.filter(x => x.navigationTemplate && this.navigationTemplates[x.navigationTemplate]).map(x => this.navigationTemplates[x].preload);
        return await Promise.all([...pages, ...templates, ...subpages].map(x => x.preload()));
    }

    generateRouting = (Providers = Fragment) => (
        <Router history={history}>
            <Providers>
                <Route render={this.onChange} /> {/* One advantage to this implementation is a single router means less overall computation on path change */}

                {Object.keys(this.redirects).map(path => (
                    <Redirect key={path} exact from={path} to={this.redirects[path]} />
                ))}
            </Providers>
        </Router>
    );

    onChange = ({ location, history }) => {
        if(location.action) {
            let { action, location: destination } = location;
            // console.log(`ATTEMPTING TO ${action}`, location);

            if(action === "PUSH") {
            //     if(destination.state && destination.state.subpage) {
                    
            //     }
            }

            return this.render({ location: destination, history });
        } else {
            return this.render({ location, history });
        }
    }

    render = ({ location, history }) => {
        // console.log(location);
        //! TODO: Is there a problem with doing this? I have no idea.
        if(location.action === "REPLACE") {
            return this.render({ location: location.location, history });
        }

        let target = matchPath(location.pathname, {
            path: this.registeredPaths,
            exact: true
        });

        if(!target) return (
            <Redirect to="/" /> //! TODO: Custom logic and/or error page support
        );

        let { uid, navigationTemplate, component: Component, tags } = this.paths[target.path];
        let Nav = this.navigationTemplates[navigationTemplate].component || Fragment;
        this.preload(tags, uid); //! TODO: Is this smart?

        return (
            <Nav {...target.params} history={history} location={location} match={target} pageId={uid}>
                <Component {...target.params} history={history} location={location} match={target} pageId={uid} />
            </Nav>
        );
    };
}

export default new PageService();
