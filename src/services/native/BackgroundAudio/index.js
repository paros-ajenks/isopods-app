import { withDeviceType } from "~/util/deviceType";

export default withDeviceType({
    default: () => import("./electron")
    // electron: () => import("./electron"),
    // mobile: () => import("./mobile"),
    // desktop: () => import("./desktop")
});
