export const play = (metadata = {}) => new Promise(resolve => {
    if(!metadata || typeof metadata.src !== "string") throw new Error("Audio source required!");

    let audio = new Audio();
    audio.preload = "all";
    audio.autoplay = false;
    audio.volume = 0;
    audio.src = metadata.src;

    audio.load();
    audio.play().then(() => resolve(audio));

    //! TODO: Media Session API
});
