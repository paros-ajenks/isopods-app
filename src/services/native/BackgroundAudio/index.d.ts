declare interface AudioArtwork {
    src: string;
    sizes: string | string[];
    type: string;
}

declare interface AudioMetadata {
    src: string;
    title: string;
    album: string;
    artist: string;
    artwork: AudioArtwork | AudioArtwork[];
}

declare interface BackgroundAudio {
    play(details: AudioMetadata): Promise<void>;
    pause(): Promise<void>;
    getInfo(): Promise<AudioMetadata>;
}

declare function load(): Promise<BackgroundAudio>;

export default load;
