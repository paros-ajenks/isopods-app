// import { Plugins } from "@capacitor/core";

alert(!!window.MediaMetadata);
export const play = src => new Promise(resolve => {
    // CapacitorMusicControls.create({
    //     track: "No One Loves Me and Neither Do I",
    //     artist: "Them Crooked Vultures",
    //     album: "Them Crooked Vultures",
    //     cover: "https://upload.wikimedia.org/wikipedia/en/3/33/ThemCrookedVulturesCover.jpg"
    // });

    let audio = new Audio();
    audio.autoplay = false;
    // audio.addEventListener("play", resolve);
    audio.src = src;

    audio.load();
    audio.play().then(resolve);
});
