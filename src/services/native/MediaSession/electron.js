/**
 * @returns {Promise<MediaSession>}
 */
const getMediaSession = () => new Promise((resolve, reject) => {
    if("mediaSession" in navigator) {
        return resolve(navigator.mediaSession);
    }

    return reject("The Media Session API is not supported on this device.");
});

export const getStatus = async() => {
    let mediaSession = await getMediaSession();
    return mediaSession.playbackState;
};

export const setPlayStatus = async(status) => {
    let mediaSession = await getMediaSession();
    mediaSession.playbackState = status;
};

export const setMetadata = async(metadata) => {
    let mediaSession = await getMediaSession();
    mediaSession.metadata = new MediaMetadata(metadata); // eslint-disable-line no-undef
};

export const getMetadata = async() => {
    let mediaSession = await getMediaSession();
    return mediaSession.metadata;
};

export const setActionHandler = async(event, handler) =>
    getMediaSession().then(mediaSession => mediaSession.setActionHandler(event, handler));
