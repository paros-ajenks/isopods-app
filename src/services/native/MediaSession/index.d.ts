declare interface AudioArtwork {
    src: string;
    sizes: string | string[];
    type: string;
}

declare interface AudioMetadata {
    src: string;
    title: string;
    album: string;
    artist: string;
    artwork: AudioArtwork | AudioArtwork[];
}

declare type MediaSessionAction = "play" | "pause" | "seekbackward" | "seekforward" | "seekto" | "previoustrack" | "nexttrack" | "skipad" | "stop";
declare type MediaSessionStatus = "playing" | "paused" | "none";

declare interface MediaSession {
    setPlayStatus(status: MediaSessionStatus): Promise<void>;
    getStatus(): Promise<MediaSessionStatus>;

    setMetadata(metadata: AudioMetadata): Promise<void>;
    getMetadata(): Promise<AudioMetadata>;

    setActionHandler(event: MediaSessionAction, handler: any): Promise<void>;
}

declare function load(): Promise<MediaSession>;

export default load;
