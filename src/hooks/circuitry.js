import React from "react";
import circuitry from "~/util/circuitry";

export const useSwitcher = (index, callback) => React.useMemo(() => circuitry.switcher(index, callback), [ index ]);

export default {
    useSwitcher
};
