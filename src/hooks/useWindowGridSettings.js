import React from "react";
import useWindowDimensions from "./useWindowDimensions";

export const withColumnCount = (displayMode = "grid") => React.useMemo(() => {
    return displayMode === "grid" ? 2 : 1;
}, [ displayMode ]);

export const withRowHeight = (displayMode = "grid") => React.useMemo(() => {
    return displayMode === "grid" ? 2 : 1;
}, [ displayMode ]);

export const useWindowGridSettings = (displayMode = "grid", data = []) => {
    let { width: windowWidth } = useWindowDimensions();
    let columns = withColumnCount(displayMode);
    let rowHeight = withRowHeight(displayMode);

    let [ settings ] = React.useState(() => ({
        columnCount: columns,
        width: windowWidth,
        columnWidth: windowWidth / columns,
        rowCount: Math.ceil(data.length / columns),
        rowHeight: rowHeight
    }));

    return settings;
};