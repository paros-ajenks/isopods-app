import React from "react";
import { observable, observe } from "mobx";

// const contexts = {};
const observables = {};

/**
 * @template T
 * @param {string} key
 * @param {T} initialvalue
 * @returns {[ React.ContextProvider<{ value?: T }>, T, (value: T) => void ]}
 */
export const useState = (key, initialValue) => {
    let onSetValue = React.useCallback(value => observables[key].set(value), [ key ]);
    let [ value, setValue ] = React.useState(initialValue);

    React.useEffect(() => {
        if(!observables[key]) {
            observables[key] = observable.box(initialValue);
        }

        return observe(observables[key], change => setValue(change.newValue));
    }, []);

    React.useEffect(() => {
        observables[key].set(value);
    }, [ value ]);

    return [ value, onSetValue ];
};

export const useContext = key => {
    let [ state, setState ] = React.useState(() => observables[key].get());

    let destroy = React.useMemo(() => observe(observables[key], change => setState(change.newValue)), [ key ]);
    React.useEffect(() => destroy, []);

    return state;
};

export const augmentsState = key => {
    let value = useContext(key);
    let setValue = React.useCallback(value => observables[key].set(value), [ key ]);

    return [ value, setValue ];
};

export const setStateValue = (key ,value) => observables[key] && observables[key].set(value);

// /**
//  * @template T
//  * @param {string} key
//  * @param {T} initialvalue
//  * @returns {[ React.ContextProvider<{ value?: T }>, T, (value: T) => void ]}
//  */
// export const providesContext = (key, initialValue) => {
//     let [ val, setVal ] = React.useState(initialValue);
//     let Context = React.useMemo(() => {
//         if(!contexts[key])
//             contexts[key] = React.createContext(initialValue);

//         return contexts[key];
//     }, []);

//     let ContextComponent = React.useCallback(({ children, ...props }) => (
//         <Context.Provider value={props.hasOwnProperty("value") ? props.value : val}>
//             {children}
//         </Context.Provider>
//     ), [ val, Context ]);

//     return [ ContextComponent, val, setVal ];
// };

// export const useContext = key => React.useContext(contexts[key]);

export default {
    useState,
    useContext,
    augmentsState,
    setStateValue
};
