import React from "react";
import { observe, get } from "mobx";

export const observes = (target, key) => {
    let [ value, setValue ] = React.useState(typeof target === "function" ? target : key !== undefined ? get(target, key) : target);

    React.useEffect(() => {
        let args = [target, key].filter(x => x !== undefined);
        return observe(...args, change => {
            setValue(change.newValue);
        });
    }, []);

    return value;
};

export default observes;
