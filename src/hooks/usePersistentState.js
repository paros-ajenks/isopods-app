import React from "react";
import LocalStorage from "~/services/LocalStorage";

/**
 * Preserves a piece of data in localStorage
 * @template T
 * @param {string} key
 * @param {T} initialValue
 * @returns {[T, React.Dispatch<T>]}
 */
export const useState = (key, initialValue) => {
    let [ value, setValue ] = React.useState(() => {
        if(LocalStorage.hasItem(key)) {
            return LocalStorage.getItem(key);
        }

        let value = typeof initialValue === "function" ? initialValue() : initialValue;
        LocalStorage.setItem(key, value);
        return value;
    });

    React.useEffect(() => {
        LocalStorage.setItem(key, value);
    }, [ value, key ]);

    return [ value, setValue ];
};

export default useState;
