import React from "react";
import usePersistentState from "./usePersistentState";

export const useToggle = (...values) => {
    let [ index, setIndex ] = React.useState(0);

    let setValue = React.useCallback((increment = true) => {
        let newValue;
        if(increment) {
            newValue = (index + 1) % values.length;
        } else {
            newValue = index - 1 < 0 ? values.length - 1 : index - 1;
        }

        return setIndex(newValue);
    }, [ index ]);

    return [ values[index], setValue ];
};

export const usePersistentToggle = (key, ...values) => {
    let [ index, setIndex ] = usePersistentState(key, 0);
    let setValue = React.useCallback(increment => {
        if(increment) {
            setIndex((index + 1) % values.length);
        } else {
            setIndex(index - 1 < 0 ? values.length - 1 : index - 1);
        }
    }, [ index, values ]);

    return [ values[index], setValue ];
};

export default {
    useToggle, usePersistentToggle
};
