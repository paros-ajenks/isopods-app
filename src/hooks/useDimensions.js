import React from "react";

export const useDimensions = ref => {
    let dimensions = React.useRef({ width: 0, height: 0 });

    React.useLayoutEffect(() => {
        if(ref.current) {
            let observer = new ResizeObserver(
                ([ event ]) => console.log(event)
            );

            observer.observe(ref.current);

            return () => observer.disconnect();
        }
    }, []);

    return dimensions.current;
};
