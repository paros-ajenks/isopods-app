import React from "react";

export const useState = (initialValue) => {
    let [ value, setValue ] = React.useState(initialValue);
    let [ cb, setCB ] = React.useState(() => {});

    React.useEffect(() => {
        if(cb) cb();
    }, [ value ]);

    let setter = React.useCallback((newValue, callback) => {
        setCB(callback);
        setValue(newValue);
    }, []);

    return [ value, setter ];
};

export default useState;
