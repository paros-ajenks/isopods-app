import React from "react";
import { nanoid } from "nanoid";

export const withId = () => React.useMemo(() => nanoid(), []);

export default withId;
