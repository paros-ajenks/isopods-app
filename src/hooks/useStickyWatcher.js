import React from "react";
import withId from "./withId";
import { observable, observe } from "mobx";

const stickyWatchers = observable({
    statuses: {},

    setStatus(key, val) {
        this.statuses[key] = val;
    },

    cleanup(key) {
        delete this.statuses[key];
    }
});

export const useStickyWatcher = () => {
    let ref = React.useRef();
    let id = withId();

    React.useLayoutEffect(() => {
        if(ref.current) {
            let observer = new IntersectionObserver(
                ([ event ]) => {
                    let isStuck = event.intersectionRatio < 1;
                    event.target.classList.toggle("is-stuck", isStuck);
                    stickyWatchers.setStatus(id, isStuck);
                },
                { threshold: [1] }
            );

            observer.observe(ref.current);
            return observer.disconnect;
        }
    }, [ ]);

    return [ ref, id ];
};

export const watchStickyElement = key => {
    let [ sticky, setSticky ] = React.useState(() => !!stickyWatchers.statuses[key]);

    React.useEffect(() => {
        if(!stickyWatchers.statuses[key]) {
            stickyWatchers.setStatus(key, false);
        }

        let destroy = observe(stickyWatchers.statuses, key, val => setSticky(val.newValue));

        return () => {
            destroy();
            stickyWatchers.cleanup(key);
        };
    }, [ key ]);

    return sticky;
};

export const useStickyElement = () => {
    let [ ref, id ] = useStickyWatcher();
    let isSticky = watchStickyElement(id);

    return [ ref, isSticky ];
};

export default useStickyWatcher;
