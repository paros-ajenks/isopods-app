import React from "react";

export const useWindowDimensions = () => {
    let [ size, setSize ] = React.useState({ width: window.innerWidth, height: window.innerHeight });

    React.useEffect(() => {
        let listener = () => {
            setSize({
                width: window.innerWidth,
                height: window.innerHeight
            });
        };

        window.addEventListener("resize", listener);

        return () => window.removeEventListener("resize", listener);
    }, [ size ]);

    return size;
};

export default useWindowDimensions;
