import { observable, observe, action } from "mobx";
import { nanoid } from "nanoid";

import LocalStorage from "~/services/LocalStorage";

const state = new class TodoState {
    todo = observable(LocalStorage.getItem("todo", []));

    get completed() {
        return this.todo.filter(x => x.done).length;
    }

    addTodo = action(message => {
        let uid = nanoid();
        let indentation = this.todo.length ? this.todo[this.todo.length - 1].indentation : 0;
        this.todo.push({ uid, indentation, message, done: false });
        return uid;
    });

    markAsDone = action((uid, done = true) => {
        this.todo.find(x => x.uid === uid).done = done;
        LocalStorage.setItem("todo", this.todo);
    });

    removeTask = action((uid) => {
        this.todo.splice(this.todo.findIndex(x => x.uid === uid), 1);
    });

    indent = action((uid) => {
        this.todo.find(x => x.uid === uid).indentation++;
        LocalStorage.setItem("todo", this.todo);
    });

    undent = action((uid) => {
        let todo = this.todo.find(x => x.uid === uid);
        todo.indentation = Math.max(0, todo.indentation - 1);
        LocalStorage.setItem("todo", this.todo);
    });
};

observe(state.todo, () => LocalStorage.setItem("todo", state.todo));

export default state;