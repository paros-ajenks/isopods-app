import { observable, observe } from "mobx";

const state = observable({
    pageTitle: observable.box(),
    shortTitle: observable.box(),
    icon: observable.box(),

    prefix: observable.box(),

    favicon: observable.box("/favicon.ico"),

    get siteFavicon() {
        return this.favicon.get();
    },

    get title() {
        let title = this.pageTitle.get();
        let prefix = this.prefix.get();

        if(prefix) {
            title = `${prefix} ${title}`;
        }

        return `${title} | Isopods`;
    },

    get pageShortTitle() {
        return this.shortTitle.get();
    },

    get pageIcon() {
        return this.icon.get();
    },

    set(title, shortTitle = this.shortTitle.get(), icon = this.icon.get()) {
        this.pageTitle.set(title);
        this.shortTitle.set(shortTitle);
        this.icon.set(icon);
    },

    setPrefix(prefix) {
        this.prefix.set(prefix);
    },

    setFavicon(favicon) {
        this.favicon.set(favicon);
    }
}, {}, { name: "PageTitleState" });

export default state;

observe(state, "title", () => {
    document.title = state.title;
});

observe(state, "siteFavicon", () => {
    let favicon = document.querySelector(`link[rel="icon"]`);
    favicon.href = state.siteFavicon;
});
