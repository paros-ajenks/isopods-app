import { observable, action, computed } from "mobx";

let audioElem = null;

const setupAudioElement = src => {
    audioElem = new Audio();
    audioElem.preload = "all";
    audioElem.crossOrigin = "anonymous";
    audioElem.src = src;
    // alert(audioElem.type);
    // audioElem.type = "audio/mpeg";
    audioElem.volume = state.volume / 100;

    audioElem.addEventListener("ended", () => state.skip(1));
    audioElem.addEventListener("error", () => {
        let ex = audioElem.error;

        alert(ex.message + ` (${ex.code}) for src ${src}`);
    });
};

const state = new class AudioState {
    @observable audioOn = false;
    @observable queue = [];
    @observable currentQueuePosition = 0;
    @observable volume = 100;

    /**
     * 0 - None
     * 1 - Queue
     * 2 - Song
     */
    @observable loopMode = 0;
    @observable shuffle = false;

    @observable currentlyPlayingFrom = "queue";
    @observable currentSource = "";
    @observable currentSourceGroup = "";

    @action setCurrentlyPlayingFrom = (src, group, type) => {
        this.currentlyPlayingFrom = type;
        this.currentSourceGroup = group;
        this.currentSource = src;
    }

    @computed
    get currentTrack() {
        return this.queue[this.currentQueuePosition];
    }

    @computed
    get isPlaying() {
        return this.currentQueuePosition !== -1 && this.queue.length > 0 && !!this.currentSource;
    }

    isPlayingAudio = url => {
        return this.isPlaying && audioElem.src === url;
    }

    @action setPlayState = (playing = !this.audioOn) => {
        console.log("Set playing state to", playing);
        this.audioOn = playing;
    };

    @action pause = () => {
        this.audioOn = false;
        return audioElem.pause();
    }

    @action play = (precache) => {
        if(audioElem) {
            return audioElem.play().then(() => this.setPlayState(true));
        }
        
        let { src } = this.currentTrack;
        setupAudioElement(src);
        let promise = audioElem.play();

        this.precache(precache);

        return promise.then(() => this.setPlayState(true));
    }

    @action skip = (amount = 1) => {
        let newPos = Math.min(this.queue.length - 1, Math.max(0, this.currentQueuePosition + amount));
        if(Math.abs(this.currentQueuePosition - newPos) < amount) newPos = -1;

        console.log(`Skipping to pos ${newPos} in queue of ${this.queue.length}`);

        this.currentQueuePosition = newPos;

        this.cleanupAudioElem();
        if(newPos !== -1) {
            return this.play();
        }
    }

    @action cleanupAudioElem = () => {
        this.audioOn = false;
        if(audioElem) {
            audioElem.pause();
            audioElem = null;
        }
    }

    @action togglePlayback = () => {
        if(audioElem.paused) {
            return audioElem.play().then(() => this.setPlayState(true));
        }

        this.audioOn = false;
        return audioElem.pause();
    }

    @action precache = (count = 2) => {
        // If we have to precache next song(s) and have something in the queue to precache
        if(count > 0 && this.currentQueuePosition + 1 < this.queue.length) {
            for(let i = 0; i < count; i++) {
                this.queue[this.currentQueuePosition + i] && new Audio(this.queue[this.currentQueuePosition + i]).load();
            }
        }
    }

    // precache determines how many songs to load in advance
    @action playTrack = (queueIndex, precache) => {
        this.currentQueuePosition = queueIndex;
        let { id, group, src } = this.currentTrack;

        this.cleanupAudioElem();
        setupAudioElement(src);
        let promise = audioElem.play();

        this.precache(precache);

        this.currentSourceGroup = group;
        this.currentSource = id;

        return promise;
    }

    @action queueItems = (items, autoplay = false) => {
        this.queue.push(...items);

        if(autoplay) {
            return this.playTrack(this.currentQueuePosition);
        }
    }

    @action setQueue = (items = [], queuePosition = 0, autoplay = false) => {
        this.queue.replace([]);
        this.currentQueuePosition = Math.min(items.length - 1, Math.max(0, queuePosition));

        console.log(`Setting queue to ${items.length} items @ pos ${queuePosition} (autoplay=${autoplay})`);

        return this.queueItems(items, autoplay);
    }
};

export default state;

console.log(null ?? false ?? "asdf");
