//? For lack of a better term, "circuitry" is just atomic, complex logic.

/**
 * Simple latch for callbacks, only updates when a specific callback parameter changes
 * @template T
 * @template U
 * @param {Number} index Callback function index to derive desired value
 * @param {(...args: T) => U} handler Callback function to be executed when desired value changes
 * @returns {(...args: T) => U} Wrapped callback
 */
export const switcher = (index, handler) => {
    let currentVal = null;

    return (...args) => {
        if(args[index] !== currentVal) {
            currentVal = args[index];
            return handler(...args);
        }
    };
};

export default {
    switcher
};
