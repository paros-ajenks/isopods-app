import React, { Suspense } from "react";

const loadLazy = (loader, fallback = "Loading...") => {
    let Component = React.lazy(loader);

    return props => (
        <Suspense fallback={fallback}>
            <Component {...props} />
        </Suspense>
    );
};

export default loadLazy;

export const prepLazy = (loader, fallback = "Loading...") => {
    let component = loadLazy(loader, fallback);

    return { component, preload: component };
};
