// It's basically https://www.npmjs.com/package/classnames but with less features. We really don't need a dependency for just the below one-liner.

/**
 * Condense a list of class names and conditional class names into a single className property value
 */
export const classNames = (...classes) => classes.reduce((classes, entry) => {
    if(Array.isArray(entry)) {
        classes.push(classNames(entry));
    } else if(typeof entry === "string" && entry.length) {
        classes.push(entry);
    } else if(typeof entry === "object") {
        let parsed = Object.keys(entry).filter(x => !!entry[x]);
        classes.push(...parsed);
    }

    return classes;
}, []).join(" ");

export default classNames;
