export default class Logger {
    name = "";

    constructor(name = "INFO") {
        this.name = name;
        this.prefix = `[${this.name}]`;
    }

    debug = (...args) => console.debug(this.prefix, ...args);
    log = (...args) => console.log(this.prefix, ...args);
    error = (...args) => console.error(this.prefix, ...args);
    warn = (...args) => console.warn(this.prefix, ...args);

    data = (obj, message) => {
        if(message) this.log(message);
        return console.table(obj);
    }
}
