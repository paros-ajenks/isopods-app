export const sanitizeURL = str => {
    if(!str) return null;

    if(/^data:/.test(str)) return str;
    
    if(/^[\w]{2,}:\/\//i.test(str))
        return new URL(str).toString();

    if(/^\//.test(str))
        return new URL(str, window.location.href).toString();

    return new URL(`https://${str}`).toString();
};

export const url = str => `url('${sanitizeURL(str)}')`;

export const getBaseURL = str => new URL(sanitizeURL(str)).origin + "/";

export default {
    url,
    sanitizeURL,
    getBaseURL
};
