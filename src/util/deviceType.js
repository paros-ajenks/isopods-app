/**
 * deviceType provides some utilities for per-device features.
 * This allows you to write platform-specific code that doesn't get imported unless you are on that device type.
 */

import React from "react";

import {
    isAndroid as AND,
    isIOS as IOS,
    isWindows as WIN,
    isMacOs as MAC,
    isMobile as MOB,
    isElectron
} from "react-device-detect";

//? Assign device type for per-platform components and general logic
let deviceType = "unknown";

if(AND) { deviceType = "android"; }
else if(IOS) { deviceType = "ios"; }
else if(WIN || MAC || !MOB) { deviceType = "desktop"; }
else if(MOB) { deviceType = "mobile"; }

export default deviceType;

export const isMobile = () => ["mobile", "android", "ios"].includes(deviceType);
// TODO: Should we perform size checks? If so, which device type do we assume as a default if "mobile" is not a specified property?
const isCurrentlyMobile = isMobile;

//? Root-level class for asset purposes.
if(isMobile()) {
    document.body.classList.add("is-mobile");
} else {
    document.body.classList.add("is-desktop");
}

/**
 * @template T
 * @param {DeviceTypeFactory<T>} opts
 * @return {T}
 */
export const withDeviceType = (opts, { sizeCheck } = {}) => {
    let def = opts.desktop || opts.default;

    if(isElectron) return opts.electron || def;
    if(deviceType === "android") return opts.android || opts.mobile || def;
    if(deviceType === "ios") return opts.android || opts.mobile || def;
    if(deviceType === "mobile" && (!sizeCheck || isCurrentlyMobile())) return opts.mobile || def;

    return def;
};

/**
 * @template T
 * @param {DeviceTypeFactory<LazyComponentFactory<T>>} components
 * @return {React.ForwardRefExoticComponent<React.RefAttributes<T>>}
 */
export const withLazyDeviceComponent = (components, fallback, opts) => {
    const Component = React.lazy(withDeviceType(components, opts));

    return React.forwardRef((props, ref) => (
        <React.Suspense fallback={fallback}>
            <Component ref={ref} {...props} />
        </React.Suspense>
    ));
};
