import React from "react";
import { observer } from "mobx-react";

import Background from "~/components/neuomorphic/Background";

import AudioState from "~/state/AudioState";

export default observer(({ src = null, mode = "queue", ...props }) => {
    let playing = !!AudioState.isPlaying;

    if(mode === "queue") {
        playing = playing && (AudioState.currentSource === src);
    } else if(mode === "album" || mode === "playlist") {
        playing = playing && (AudioState.currentSourceGroup === src);
    }

    console.log(src, mode, playing);

    return (
        <Background {...props} active={playing} />
    );
});
