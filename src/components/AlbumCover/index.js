import React from "react";
import classNames from "~/util/classnames";
import { url } from "~/util/URLParser";

import Primitive from "~/components/neuomorphic/Primitive";

import "./style.scss";

const Component = React.forwardRef(({
    visible, src
}, ref) => (
    <div ref={ref} className={classNames("album-cover", !visible && "is-hidden")} style={{ backgroundImage: url(src) }} />
));

const SquareComponent = ({ className, art, visible }, ref) => (
    <Primitive shape="square" className={className} ref={ref}>
        {typeof art === "function" ? art() : (
            <Component src={art} visible={visible} />
        )}
    </Primitive>
);

export const SquareAlbumCover = React.forwardRef(SquareComponent);
export default Component;
