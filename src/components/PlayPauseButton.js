import React from "react";
import { observer } from "mobx-react";

import Button from "~/components/neuomorphic/Button";

import AudioState from "~/state/AudioState";

export default observer(({ src = null, mode = "queue", ...props }) => {
    let playing = !!AudioState.isPlaying;

    if(mode === "queue") {
        playing = playing && (AudioState.currentSource === src);
    } else if(mode === "album" || mode === "playlist") {
        playing = playing && (AudioState.currentSourceGroup === src);
    }

    // playing = playing && AudioState.audioOn;

    let icon = playing ? "pause" : "play_arrow";

    return (
        <Button {...props} toggleable icon={icon} active={playing} />
    );
});
