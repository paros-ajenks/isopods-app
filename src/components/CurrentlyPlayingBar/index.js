import React from "react";
import classNames from "~/util/classnames";

import { SquareAlbumCover } from "~/components/AlbumCover";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
import { motion } from "framer-motion";

import "./style.scss";

const Component = ({
    className,
    playing = false,
    onTogglePlay,
    album: { id, art, title, subtitle } = {}
}, ref) => (
    <Navbar className={classNames("currently-playing-bar", className)} ref={ref}>
        <div className="details">
            <SquareAlbumCover className="is-flat" art={art} visible />
            <div className="track-info">
                <p className="title">{title}</p>
                <p className="subtitle">{subtitle}</p>
            </div>
        </div>
        <div className="actions">
            <Button variant="icon-square" className="has-m-0" icon={playing ? "pause" : "play_arrow"} size="md" onClick={onTogglePlay} />
        </div>

        <motion.div animate={{ width: "100%" }} className="progress-indicator" />
    </Navbar>
);

export default React.forwardRef(Component);
