export default time => {
    if(time && time.value) time = time.value;
    if(!time) return "0:00";

    let h = Math.floor(time / 3600), m = Math.floor(time / 60) % 60, s = time % 60;

    return [h,m,s]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
        .replace(/^0+(\d)/gmi, (_,d) => d);
};
