import ReactDOM from "react-dom";

const DEFAULT_ROOT = document.querySelector("#root");
export default ({ children, id, parent = DEFAULT_ROOT }) => ReactDOM.createPortal(children, parent, id);
