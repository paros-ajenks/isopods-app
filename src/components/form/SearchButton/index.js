import React from "react";
import classNames from "~/util/classnames";
import Toggles from "~/hooks/useToggle";

import Button from "~/components/neuomorphic/Button";

import "./style.scss";

const Component = ({
    className,
    onSearch,
    placeholder = "Search"
}, ref) => {
    let [ isOpen, toggleOpen ] = Toggles.useToggle(false, true);
    let [ text, setText ] = React.useState("");
    let input = React.useRef();

    let onClick = React.useCallback(e => {
        if(!isOpen) {
            input.current.focus();
            return toggleOpen();
        }

        return text.length && onSearch(text);
    }, [ isOpen, text ]);

    let onClear = React.useCallback(() => {
        input.current.focus();
        setText("");
        return onSearch(null);
    }, [ setText ]);

    // let onBlur = React.useCallback(e => toggleOpen(), [ isOpen ]);

    let onSubmit = React.useCallback(e => {
        e.preventDefault();
        return onSearch(text);
    }, [ text ]);

    return (
        <form className={classNames("search-button", className, isOpen && "is-open")} ref={ref} onSubmit={onSubmit}>
            <div className="input-area">
                <input ref={input} type="text" value={text} onChange={e => setText(e.target.value)} placeholder={placeholder} />
                {!!text.length && (
                    <Button variant="icon" iconVariant="outline" className="is-flat has-m-0" icon="close" onClick={onClear} />
                )}
            </div>
            <Button variant="icon" iconVariant="outline" className="is-flat has-m-0" icon="search" size="md" onClick={onClick} />
        </form>
    );
};

export default React.forwardRef(Component);
