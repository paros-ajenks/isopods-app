import React from "react";
import classNames from "~/util/classnames";

import TrackDisplayItem from "../TrackDisplayItem";

const Component = ({
    className,
    items = [],
    keyProp = "key",
    itemComponent: ItemComponent = TrackDisplayItem,
    selectedItems = [],
    activeItem,
    children,
    itemProps = {},
    allowSelections,
    onSelectItem,
    onPickItem,
    ...props
}, ref) => (
    <div className={classNames("track-list", className)} ref={ref} {...props}>
        {children}
        {items.map(item => (
            <ItemComponent {...itemProps} key={item[keyProp]}
                active={activeItem === item[keyProp]}
                selected={selectedItems && selectedItems.includes(item[keyProp])}
                value={item} keyProp={keyProp}
                allowSelections={allowSelections}
                onSelectItem={onSelectItem}
                onPickItem={onPickItem} />
        ))}
    </div>
);

export default React.forwardRef(Component);
