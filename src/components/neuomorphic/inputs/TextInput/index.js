import React from "react";
import { nanoid } from "nanoid";

import "./style.scss";

export default React.forwardRef(({
    type = "text",
    beforeAdornment,
    afterAdornment,
    id: ID,
    ...props
}, ref) => {
    const id = React.useMemo(() => ID || nanoid(8), [ ID ]);

    return (
        <div className="input">
            {beforeAdornment && (
                <label className="adornment" htmlFor={id}>{beforeAdornment}</label>
            )}
            <input ref={ref} type={type} {...props} id={id} />
            {afterAdornment && (
                <label className="adornment" htmlFor={id}>{afterAdornment}</label>
            )}
        </div>
    );
});
