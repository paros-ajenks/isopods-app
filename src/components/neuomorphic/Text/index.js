import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

const Component = ({
    as: Elem = "p",
    bold,
    italic,
    underlined,
    className,
    children,
    content,
    ...props
}, ref) => (
    <Elem ref={ref} {...props} className={classNames(
        "text", className,
        bold && "is-bold",
        italic && "is-italic",
        underlined && "is-underlined"
    )} children={content || children} />
);

export default React.forwardRef(Component);
