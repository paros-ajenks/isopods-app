import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";
import { usesStylesheet } from "~/hooks/usesStylesheet";

const sheets = {
    "normal": "https://fonts.googleapis.com/icon?family=Material+Icons",
    "outline": "https://fonts.googleapis.com/icon?family=Material+Icons+Outlined",
    "round": "https://fonts.googleapis.com/icon?family=Material+Icons+Round",
    "twotone": "https://fonts.googleapis.com/icon?family=Material+Icons+Two+Tone",
    "sharp": "https://fonts.googleapis.com/icon?family=Material+Icons+Sharp"
};

const Component = ({
    as: As = "i",
    variant = "normal",
    className,
    icon,
    ...props
}, ref) => {
    usesStylesheet(sheets[variant]);

    return (
        <As ref={ref} className={classNames("icon", `variant-${variant}`, className)} {...props}>{icon}</As>
    );
};

export default React.forwardRef(Component);
