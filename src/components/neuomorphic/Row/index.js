import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

const Component = ({ as: As = "div", className, ...props }, ref) => (
    <As ref={ref} {...props} className={classNames("row", className)} />
);

export default React.forwardRef(Component);
