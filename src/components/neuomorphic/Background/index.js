import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

export default ({ active, ...style }) => (
    <div className={classNames("background", active && "is-active")} style={style} />
);
