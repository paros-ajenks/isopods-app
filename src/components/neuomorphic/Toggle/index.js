import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

const Component = ({
    className,
    checked,
    label,
    onChange,
    ...props
}, ref) => (
    <label className={classNames("toggle", checked && "is-checked", className)} onClick={e => onChange && onChange(!checked)}>
        <div className="toggle-object">
            <input type="checkbox" ref={ref} {...props} checked={checked} onChange={e => onChange && onChange(!e.target.checked)} />
            <div className="indicator" />
        </div>
        {label && (
            <div className="label">{label}</div>
        )}
    </label>
);

export default React.forwardRef(Component);
