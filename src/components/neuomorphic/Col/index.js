import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

const Component = ({
    as: As = "div",
    sizes = [],
    size,
    span,
    className,
    ...props
}, ref) => {
    let col = "col";

    if(size) col += `-${size}`;
    if(span) col += `-${span}`;

    return (
        <As ref={ref} {...props} className={classNames(col, ...sizes.map(x => `col-${x}`), className)} />
    );
};

export default React.forwardRef(Component);
