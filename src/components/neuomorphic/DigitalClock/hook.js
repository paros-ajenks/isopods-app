import { useState, useEffect } from "react";

const date = () => +new Date();

const seg = n => n.toString().padStart(2, "0");

const getDate = () => {
    let date = new Date();

    let h = (date.getHours() % 12) || 12;
    let m = date.getMinutes();
    let s = date.getSeconds();
    let p = date.getHours() >= 12 ? "P" : "A";

    return `${seg(h)}:${seg(m)}:${seg(s)} ${p}`;
};

const getStartDelay = () => {
    let start = Math.ceil(date() / 1000) * 1000;
    return start - date();
};

export const withTime = () => {
    let [ val, update ] = useState(getDate());

    useEffect(() => {
        let diff = getStartDelay();
        let timer;

        setTimeout(() => {
            timer = setInterval(() => update(getDate()), 1000);
            update(getDate());
        }, diff);

        return () => clearInterval(timer);
    }, []);

    return val;
};
