import React from "react";
import classNames from "~/util/classnames";

import { withTime } from "./hook";

import "./style.scss";

const Component = ({
    className,
    ...props
}, ref) => {
    let time = withTime();

    return (
        <div ref={ref} className={classNames("digital-clock", className)} {...props}>
            <p className="clock-text">{time}</p>
        </div>
    );
};

export default React.forwardRef(Component);
