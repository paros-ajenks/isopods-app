import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

export default ({
    className,
    variant = "normal",
    ...props
}) => (
    <nav className={classNames("navbar-bottom", variant && `is-${variant}`, className)} {...props} />
);
