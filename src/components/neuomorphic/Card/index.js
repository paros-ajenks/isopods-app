import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

const Component = ({ as: Elem = "div", className, ...props }, ref) => (
    <Elem ref={ref} {...props} className={classNames("card", className)} />
);

export default React.forwardRef(Component);
export { default as CardHeader } from "./CardHead";
export { default as CardBody } from "./CardBody";