import React from "react";
import classNames from "~/util/classnames";

import Portal from "~/components/Portal";

import "./style.scss";

const Component = ({
    area = "bottom-right",
    className,
    children,
    innerProps = {},
    ...props
}, ref) => (
    <Portal>
        <div className={classNames("floating-area", `is-${area}`, className)} ref={ref} {...props}>
            <div {...innerProps} className="ui-layer">{children}</div>
        </div>
    </Portal>
);

export default React.forwardRef(Component);
