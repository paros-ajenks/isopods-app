import React from "react";
import classNames from "~/util/classnames";

import "./style.scss";

export default ({
    className,
    variant = "normal",
    clickable,
    onClick,
    ...props
}) => {
    let handleClick = React.useCallback(e => {
        e.persist();
        if(clickable)
            return onClick && onClick(e);
    }, [ clickable, onClick ]);

    return (
        <nav className={classNames("navbar", variant && `is-${variant}`, clickable && "is-clickable", className)}
            onClick={handleClick} {...props} />
    );
};
