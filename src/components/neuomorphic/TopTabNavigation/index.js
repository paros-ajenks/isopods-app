import React from "react";
import classNames from "~/util/classnames";

import { motion, AnimateSharedLayout } from "framer-motion";

import "./style.scss";

const Component = ({
    tabs = [],
    selectedTab = 0,
    onSelectTab,
    className,
    tabClassName,
    activeTabClassName,
    disabled,
    ...props
}, ref) => (
    <AnimateSharedLayout>
        <div className={classNames("top-tab-nav", className)} {...props} ref={ref}>
            {tabs.map(tab => (
                <React.Fragment key={`toptabnav-${tab.id}`}>
                    <button className={classNames("tab", tabClassName, tab.className, {
                        "is-active": selectedTab === tab.id,
                        "is-disabled": disabled || tab.disabled
                    })} onClick={() => (!disabled && !tab.disabled) && onSelectTab && onSelectTab(tab)}>
                        {tab.label}

                        {selectedTab === tab.id && (
                            <motion.div className="indicator" layoutId="indicator" transition={{ duration: .3 }} />
                        )}
                    </button>
                </React.Fragment>
            ))}
        </div>
    </AnimateSharedLayout>
);

export default React.forwardRef(Component);
