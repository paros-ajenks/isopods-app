import React from "react";
import classNames from "~/util/classnames";

import Icon from "~/components/neuomorphic/Icon";

import "./style.scss";

const ButtonIcon = ({
    toggleable,
    active,

    icon,
    iconVariant,

    activeIcon,
    activeIconVariant,

    inactiveIcon,
    inactiveIconVariant,

    iconClassName
}) => (
    <Icon variant={(toggleable && (active ? activeIconVariant : inactiveIconVariant)) || iconVariant}
        icon={(toggleable && (active ? activeIcon : inactiveIcon)) || icon} className={iconClassName} />
);

const Component = ({
    as: As = "button",
    className,
    type = "button",
    variant = "normal", // "normal", "rounded", "icon"
    toggleable,
    active,
    activeIcon,
    activeIconVariant,
    inactiveIcon,
    inactiveIconVariant,
    icon,
    iconVariant,
    iconComponent: IconComponent = ButtonIcon,
    iconClassName,
    label,
    style,
    color,
    size = "sm",
    ...props
}, ref) => (
    <As ref={ref} {...props} type={type} className={classNames("btn", `is-${size}`, `is-${variant}`, toggleable && active && "is-active", className)} style={{ ...style, "--btn-color": color }}>
        {(icon || toggleable) && (
            <IconComponent {...{toggleable, active, icon, iconVariant, activeIcon, activeIconVariant, inactiveIcon, inactiveIconVariant, iconClassName}} />
        )}
        {label && (
            <span className="btn-label">{label}</span>
        )}
    </As>
);

export default React.forwardRef(Component);
