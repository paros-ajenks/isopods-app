import React from "react";
import { isFragment } from "react-is";
import { ROUTE_MARK } from "./common";

const SwitchWrapped = ({ render }) => {
    let res = render();

    if(Array.isArray(res.props.children)) {
        return res.props.children;
    } else if(React.isValidElement(res.props.children)) {
        return [res.props.children];
    }

    return [];
};

const expand = (node, depth = 0) => {
    let children = [];

    if(depth === 3) return []; // TODO: See if we can lower it to 2
    if(!node || !node.props || !node.props.children) return [];

    React.Children.forEach(node.props.children, (child, i) => {
        if(!child || !React.isValidElement(child)) return;

        if(isFragment(child)) {
            let merged = expand(child, depth + 1); // Search through fragment for relevant routes
            children.push(...merged);
        } else if(child.type && child.type[ROUTE_MARK]) {
            children.push(...SwitchWrapped({ render: child.type })); // Found another cluster
        } else {
            children.push(child); // Append potential route
        }
    });

    return children;
};

//? BetterSwitch - Shim to customize clustered route definitions for react-router-dom
export default (...views) => {
    let root = { props: { children: views.map(x => x()) } }; // Execute each cluster to get the children
    return expand(root);
};
