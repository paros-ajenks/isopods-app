export const ROUTE_MARK = Symbol("is-valid-react-route");

export const withRoutes = func => {
    func[ROUTE_MARK] = true;
    return func;
};
