import React from "react";
import { Route } from "react-router-dom";
import state from "~/state/PageTitleState";

export default ({ title, shortTitle, icon, component: Component, preloads, ...props }) => (
    <Route {...props} component={({ match, ...otherProps }) => {
        state.set(title, shortTitle, icon);

        // Object.keys(match.params).forEach(key => {
        //     if(typeof match.params[key] === "string" && match.params[key].includes("#")) {
        //         match.params[key] = match.params[key].split("#")[0];
        //     }
        // });

        return (
            <Component {...otherProps} match={match} {...match.params} />
        );
    }} />
);
