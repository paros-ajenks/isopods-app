import { observable, action, computed, toJS } from "mobx";
import { nanoid } from "nanoid";

export default new class TodoState {
    @observable lists = [];
    @observable listData = [];

    @observable activeList = "";

    @computed
    get currentList() {
        return this.lists.find(x => x.id === this.activeList);
    }

    @action setActiveList = async(id) => {
        if(this.activeList) {
            let list = this.currentList;

            if(list) {
                list.data.replace(toJS(this.listData));
            }
        }

        let list = this.lists.find(x => x.id === id);

        if(list) {
            this.activeList = id;
            this.listData.replace(toJS(list.data));
        }
    }

    @action addList = ({ name }) => {
        this.lists.push({
            id: nanoid(32), name, data: []
        });
    }
};
