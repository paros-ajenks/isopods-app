import React from "react";
import classNames from "~/util/classnames";

import Row from "~/components/neuomorphic/Row";

import "./style.scss";

const Component = ({ className, ...props }, ref) => ( //has-pt-3 has-justified-between has-px-3
    <Row ref={ref} className={classNames("action-sub-bar", className)} {...props} />
);

export default React.forwardRef(Component);
