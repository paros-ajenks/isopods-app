import React from "react";
import { observer } from "mobx-react";

import Icon from "~/components/neuomorphic/Icon";

import AudioState from "~/state/AudioState";

export default observer(({ src = null, mode = "queue" }) => {
    let playing = AudioState.isPlaying;

    if(mode === "queue") {
        playing &= (AudioState.currentSource === src);
    } else if(mode === "album" || mode === "playlist") {
        playing &= (AudioState.currentSourceGroup === src);
    }

    return (
        <Icon icon={playing ? "pause" : "play_arrow"} />
    );
});
