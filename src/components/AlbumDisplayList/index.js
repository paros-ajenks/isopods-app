import React from "react";
import classNames from "~/util/classnames";
// import useWindowGridSettings from "~/hooks/useWindowGridSettings";

import Primitive from "~/components/neuomorphic/Primitive";
// import { FixedSizeGrid as Grid } from "react-window";
import Icon from "~/components/neuomorphic/Icon";
import Col from "~/components/neuomorphic/Col";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import AlbumCover from "../AlbumCover";

import "./style.scss";

const handleClick = (e, onClick) => {
    e.persist();
    if(onClick) return onClick(e);
};

const AddNewButton = ({ onClickAlbum, text = "Create Playlist" }) => (
    <Col as={motion.div} initial={false} className="album-wrapper is-add-new" span="6">
        <div className="album is-clickable" onClick={e => handleClick(e, onClickAlbum)}>
            <Primitive shape="square" className="album-cover is-flat">
                <Icon icon="library_add" className="add-new-icon" />
            </Primitive>
            <div className="album-info">
                <span className="album-title">{text}</span>
            </div>
        </div>
    </Col>
);

const AlbumItem = ({ className, ...album }, ref) => (
    <Col ref={ref} className={classNames("album-wrapper", className)} span="6">
        <Link to={`/album/${album.id}`} className="album">
            <Primitive shape="square" className="album-cover is-flat">
                {typeof album.art === "function" ? album.art() : (
                    <AlbumCover src={album.art} visible={!("visible" in album) || album.visible} />
                )}
            </Primitive>
            <div className="album-info">
                <span className="album-title">{album.title}</span>
                {album.subtitle && (
                    <span className="album-subtitle">{album.subtitle}</span>
                )}
            </div>
        </Link>
    </Col>
);

const Component =  ({
    className,
    albumClassName,
    albums = [],
    onAddNew,
    onClickAlbum,
    displayMode = "grid",
    topArea,
    children,
    ...props
}, ref) => {
    // let gridSettings = useWindowGridSettings(displayMode, albums);

    return (
        <div className={classNames("album-display-list", `display-is-${displayMode}`, className)} ref={ref} {...props}>
            {/* <Grid {...gridSettings} /> */}
            {onAddNew && (
                <AddNewButton onClickAlbum={onAddNew} text="Create Playlist" />
            )}

            {topArea}

            {albums.map(album => (
                <Item key={`album-${album.id}`} className={albumClassName} {...album} />
            ))}

            {children}
        </div>
    );
};

export default React.forwardRef(Component);
export const Item = React.forwardRef(AlbumItem);
