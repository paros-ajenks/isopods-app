import { observer } from "mobx-react";

import AudioState from "~/state/AudioState";

export default observer(({ src = null, mode = "queue" }) => {
    let playing = AudioState.isPlaying;

    if(mode === "queue") {
        playing &= (AudioState.currentSource === src);
    } else if(mode === "album" || mode === "playlist") {
        playing &= (AudioState.currentSourceGroup === src);
    }

    return playing ? "pause" : "play_arrow";
});
