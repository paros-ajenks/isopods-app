import "./assets";

import ReactDOM from "react-dom";
import App from "./views";

// import "./util/deviceType";

// import getBackgroundAudio from "~/services/native/BackgroundAudio";
// import getMediaSession from "~/services/native/MediaSession";

// const initializeCallbacks = async() => {
//     let MediaSession = await getMediaSession();

//     MediaSession.setActionHandler("play", console.log);
//     MediaSession.setActionHandler("pause", console.log);
//     MediaSession.setActionHandler("nexttrack", console.log);
//     MediaSession.setActionHandler("previoustrack", console.log);
// };

// (async() => {
    // let BackgroundAudio = await getBackgroundAudio();
    // let MediaSession = await getMediaSession();

    // let metadata = {
    //     src: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
    //     title: "Battery Acid",
    //     album: "Era Vulgaris",
    //     artist: "Queens of the Stone Age",
    //     artwork: [{
    //         src: "https://images-na.ssl-images-amazon.com/images/I/517HpPUIsxL._SX425_.jpg",
    //         sizes: "425x425",
    //         type: "image/jpg"
    //     }]
    // };

    // await initializeCallbacks();

    // await MediaSession.setMetadata(metadata);
    // await BackgroundAudio.play(metadata);
    // await MediaSession.setPlayStatus("playing");
// })();

ReactDOM.render(App, document.querySelector("#root"));
