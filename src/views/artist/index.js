import React from "react";
// import classNames from "~/util/classnames";
import { url } from "~/util/URLParser";

// import TextInput from "~/components/neuomorphic/inputs/TextInput";
import Primitive from "~/components/neuomorphic/Primitive";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
// import Toggle from "~/components/neuomorphic/Toggle";
// import Card from "~/components/neuomorphic/Card";
// import Icon from "~/components/neuomorphic/Icon";
import Text from "~/components/neuomorphic/Text";
// import Col from "~/components/neuomorphic/Col";
import Row from "~/components/neuomorphic/Row";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

let artist = {
    name: "The GOASTT (Ghost Of A Saber Tooth Tiger)",
    background: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Sean_Lennon_and_The_Ghost_of_a_Saber_Tooth_Tiger_-_WeekEnd_des_Curiosit%C3%A9s_2015-3845_01.jpg/1280px-Sean_Lennon_and_The_Ghost_of_a_Saber_Tooth_Tiger_-_WeekEnd_des_Curiosit%C3%A9s_2015-3845_01.jpg",
    totalAlbums: 5,
    recentAlbums: [],
    relatedArtists: [
        { name: "Moon Duo", background: "ASFASF" }
    ]
};

export default () => {
    let backgroundStyle = {
        backgroundImage: url(artist.background),
        backgroundSize: "cover"
    };

    let innerStyle = {
        background: "rgba(0, 0, 0, 0.7)"
    };

    return (
        <React.Fragment>
            <Navbar className="is-flex has-aligned-center has-justified-between is-z-1">
                <Button variant="icon-square" icon="keyboard_arrow_left" className="has-m-0 is-inline-flex" />

                <div>
                    <Button variant="icon-square" icon="search" className="has-m-0 has-mr-4 is-inline-flex" />
                    <Button variant="icon-square" icon="more_vert" className="has-m-0 is-inline-flex" />
                </div>
            </Navbar>

            <Row>
                <Primitive shape="rect-2-1" style={backgroundStyle} innerProps={{ style: innerStyle }}>
                    <Text as="h4" className="artist-title">{artist.name}</Text>
                </Primitive>
            </Row>

            <Row className="has-px-3">
                <Text as="h4">Albums ({artist.totalAlbums})</Text>
            </Row>
            <Row>
                
            </Row>

            <Row className="has-px-3">
                <Text as="h4">Related Artists</Text>
            </Row>
            <Row>
                
            </Row>
        </React.Fragment>
    );
};
