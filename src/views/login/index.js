import React from "react";
// import classNames from "~/util/classnames";

import TextInput from "~/components/neuomorphic/inputs/TextInput";
// import Primitive from "~/components/neuomorphic/Primitive";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
// import Toggle from "~/components/neuomorphic/Toggle";
// import Card from "~/components/neuomorphic/Card";
import Icon from "~/components/neuomorphic/Icon";
// import Text from "~/components/neuomorphic/Text";
// import Col from "~/components/neuomorphic/Col";
import Row from "~/components/neuomorphic/Row";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

export default () => {
    return (
        <React.Fragment>
            <Navbar className="is-flex has-aligned-center has-justified-between" variant="sticky">
                Isopods
            </Navbar>
            <Row className="top-section">
                <div className="login-section">
                    <TextInput placeholder="Username" beforeAdornment={<Icon icon="person" />} />
                    <TextInput placeholder="Password" type="password" beforeAdornment={<Icon icon="lock" />} />

                    <Button variant="rounded" label="Login" className="login-btn" color="#FFAB00" />
                </div>
            </Row>
        </React.Fragment>
    );
};
