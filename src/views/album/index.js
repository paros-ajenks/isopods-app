import React from "react";
import classNames from "~/util/classnames";

import PlayPauseBackground from "~/components/PlayPauseBackground";
import PlayPauseButton from "~/components/PlayPauseButton";
import Primitive from "~/components/neuomorphic/Primitive";
import Timestamp from "~/components/display/Timestamp";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
// import Toggle from "~/components/neuomorphic/Toggle";
import AlbumCover from "~/components/AlbumCover";
// import Icon from "~/components/neuomorphic/Icon";
// import Text from "~/components/neuomorphic/Text";
// import Col from "~/components/neuomorphic/Col";
import Row from "~/components/neuomorphic/Row";
import SongList from "./components/SongList";

import AudioState from "~/state/AudioState";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

const albumDetails = {
    id: "OLAK5uy_kGM9j5cC9bPGtORqIZ7zp5g27at-3L2RA",
    name: "Era Vulgaris",
    artist: "Queens of the Stone Age",
    image: "https://images-na.ssl-images-amazon.com/images/I/517HpPUIsxL._SX425_.jpg",
    year: 2007,
    tracks: [
        { id: "SxVtgulIO5k", name: "Turnin' On The Screw",  duration: 320 },
        { id: "lnPdgvAAfSg", name: "Sick, Sick, Sick",      duration: 214 },
        { id: "fRBgqqicVNo", name: "I'm Designer",          duration: 244 },
        { id: "4EWysj7SrcQ", name: "Into The Hollow",       duration: 222 },
        { id: "HZcfK0jzAj4", name: "Misfit Love",           duration: 339 },
        { id: "w6k1gTP1Pdw", name: "Battery Acid",          duration: 246 },
        { id: "hm6RQgGTZr0", name: "Make It Wit Chu",       duration: 290 },
        { id: "HBo_R-MRazU", name: "3's & 7's",             duration: 214 },
        { id: "TsFwRCBDkh4", name: "Suture Up Your Future", duration: 277 },
        { id: "j2GP3roZk5Y", name: "River In The Road",     duration: 199 },
        { id: "WJZSdU-Iju0", name: "Run, Pig, Run",         duration: 279 }
    ]
};

const totalTime = albumDetails.tracks.reduce((a,b) => a + b.duration, 0);

export default class AlbumView extends React.Component {
    state = { favorited: false };

    toggleFavorite = () => this.setState({ favorited: !this.state.favorited });

    onPlayAlbum = () => {
        let promise;
        if(AudioState.currentSourceGroup === albumDetails.id) {
            promise = AudioState.togglePlayback();
        } else {
            console.log("Playing album...");

            AudioState.setCurrentlyPlayingFrom(null, albumDetails.id, "album");
            promise = AudioState.setQueue(albumDetails.tracks.map(x => ({ id: x.id, group: albumDetails.id, src: `http://10.0.0.14:8080/audio/youtube/${x.id}` })), 0, true);
        }

        if(promise) {
            console.log(promise);
            promise.catch(console.error);
        }
    }

    onClickTrack = index => async(e) => {
        e.preventDefault();
        e.stopPropagation();

        AudioState.setCurrentlyPlayingFrom(null, albumDetails.id, "album");
        let promise = AudioState.setQueue(albumDetails.tracks.map(x => ({ id: x.id, group: albumDetails.id, src: `http://10.0.0.14:8080/audio/youtube/${x.id}` })), index, true);

        if(promise) {
            console.log(promise);
            promise.catch(console.error);
        }
    }

    onClickOptions = track => async(e) => {
        e.preventDefault();
        e.stopPropagation();

        console.log("Clicked options on track!");
    }

    render() {
        let { favorited } = this.state;

        return (
            <React.Fragment>
                <PlayPauseBackground mode="album" src={albumDetails.id} background="linear-gradient(315deg, rgba(204,28,116,0.2) 0%, rgba(255,255,255,0) 70%)" transition="all 1s ease" />

                <Navbar className="is-flex has-aligned-center has-justified-between is-z-1" variant="sticky">
                    <Button variant="icon-square" icon="keyboard_arrow_left" className="has-m-0 has-mr-4 is-inline-flex" />

                    <div>
                        <Button variant="icon-square"
                            inactiveIcon="favorite_border"
                            icon="favorite"
                            className={classNames("has-m-0 has-mr-4 is-inline-flex", favorited && "is-tertiary-text")}
                            active={favorited}
                            toggleable
                            onClick={this.toggleFavorite} />

                        <Button variant="icon-square" icon="more_vert" className="has-m-0 is-inline-flex" />
                    </div>
                </Navbar>

                <Row className="has-justified-center has-aligned-start is-relative">
                    <Primitive className="album-image">
                        <AlbumCover visible src={albumDetails.image} />
                        <PlayPauseButton mode="album" src={albumDetails.id} className="play-album-btn" variant="icon" size="lg" onClick={this.onPlayAlbum} />
                    </Primitive>
                </Row>

                <Row className="album-details">
                    <h2 className="album-title">{albumDetails.name}</h2>
                    <h4 className="artist">{albumDetails.artist}</h4>
                    <h5 className="duration">{albumDetails.year} - {albumDetails.tracks.length} songs - {Timestamp(totalTime)}</h5>
                </Row>

                <Row className="album-tracks">
                    <SongList albumDetails={albumDetails} onClickOptions={this.onClickOptions} onClickTrack={this.onClickTrack} />
                </Row>
            </React.Fragment>
        );
    }
}
