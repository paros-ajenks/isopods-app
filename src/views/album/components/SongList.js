import React from "react";
import { observer } from "mobx-react";
import classNames from "~/util/classnames";

import Timestamp from "~/components/display/Timestamp";
import Button from "~/components/neuomorphic/Button";
import Card from "~/components/neuomorphic/Card";

import AudioState from "~/state/AudioState";

export default observer(({
    albumDetails,
    onClickTrack,
    onClickOptions
}) => albumDetails.tracks.map((track, i) => {
    let playing = false;

    if(AudioState.currentSource === track.id && AudioState.currentSourceGroup === albumDetails.id)
        playing = true;

    return (
        <Card key={track.id} className={classNames("btn", playing ? "is-playing is-active" : "is-flat")} role="button" type="button" onClick={onClickTrack(i)}>
            <span className="place">{i + 1}</span>
            <span className="title">{track.name}</span>
            <span className="duration">{Timestamp(track.duration)}</span>
            <Button className="actions is-flat" variant="square-icon" size="sm" icon="more_vert" onClick={onClickOptions(track)} />
        </Card>
    );
}));
