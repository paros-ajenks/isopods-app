import React from "react";
import classNames from "~/util/classnames";

// import Primitive from "~/components/Primitive";
import DigitalClock from "~/components/neuomorphic/DigitalClock";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
import Toggle from "~/components/neuomorphic/Toggle";
import Card from "~/components/neuomorphic/Card";
import Icon from "~/components/neuomorphic/Icon";
import Text from "~/components/neuomorphic/Text";
import Col from "~/components/neuomorphic/Col";
import Row from "~/components/neuomorphic/Row";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

let initialState = [
    { id: 1, active: false, time: "9:00 pm"  },
    { id: 2, active: false, time: "9:07 pm"  },
    { id: 3, active: true,  time: "9:15 pm"  },
    { id: 4, active: true,  time: "9:22 pm"  },
    { id: 5, active: false, time: "9:30 pm"  },
    { id: 6, active: false, time: "9:37 pm"  },
    { id: 7, active: false, time: "9:45 pm"  },
    { id: 8, active: true,  time: "9:52 pm"  },
    { id: 9, active: true,  time: "10:00 pm" }
];

export default () => {
    let [ alarms, setAlarms ] = React.useState(initialState);
    
    const onChange = (alarm, active) => {
        let newAlarms = [...alarms];
        let index = newAlarms.findIndex(x => x.id === alarm.id);

        if(index !== -1) {
            newAlarms[index].active = active;
            setAlarms(newAlarms);
        }
    };

    return (
        <React.Fragment>
            <Navbar className="is-flex has-aligned-center has-justified-between has-pb-1">
                <Text as="h3" bold className="has-m-0 is-inline">Clock</Text>
                <Button variant="icon-square" icon="settings" className="has-m-0 is-inline-flex" />
            </Navbar>

            <Row className="has-justified-center has-aligned-start">
                <Col sizes={["md-6", "lg-4", "xl-3"]}>
                    <DigitalClock />
                </Col>
                <Col sizes={["md-6", "lg-4", "xl-3"]}>
                    {alarms.map(alarm => (
                        <Card key={`alarm-${alarm.id}`} className="alarm-card">
                            <span className={classNames("is-inline-flex has-aligned-center is-smooth", alarm.active ? "is-tertiary-text" : "is-secondary-text")}>
                                <Icon icon="alarm" className="has-mr-2" />
                                {alarm.time}
                            </span>
                            <Toggle checked={alarm.active} onChange={active => onChange(alarm, active)} />
                        </Card>
                    ))}
                </Col>
            </Row>
        </React.Fragment>
    );
};
