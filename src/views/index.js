/**
 *? Contained within this file are several different experiments.
 *? The goal is to figure out how to design React apps to be extremely mobile-friendly, to the point where they feel no different from a native application
 *? That means smooth transitions, infinite scrolling, ever-present navigation components, ZERO PAGE LOADING ANIMATIONS, etc.
 *? You should be able to install this as a PWA and see zero distinctions between this and your other apps.
 */

// import React from "react";
// import loadLazy from "~/util/lazy";
// import { Router, Switch, Redirect } from "react-router-dom";
// import history from "~/services/BrowserHistory";
// import Route from "~/components/routing/ViewRoute";

// const MusicView = loadLazy(() => import("./music"));
// const LibraryView = loadLazy(() => import("./library"));
// const AlbumView = loadLazy(() => import("./album"));
// const LoginView = loadLazy(() => import("./login"));
// const ArtistView = loadLazy(() => import("./artist"));

//! ====================== Experiment 1 - Pretty much a standard react-router-dom implementation.

// export default (
//     <Router history={history}>
//         <Switch>
//             <Route exact path="/library"
//                 title="My Library" shortTitle="Library"
//                 component={LibraryView} />

//             <Route exact path="/music" component={MusicView} />
//             <Route exact path="/album" component={AlbumView} />
//             <Route exact path="/login" component={LoginView} />
//             <Route exact path="/artist" component={ArtistView} />

//             <Redirect exact from="/" to="/library" />
//         </Switch>

//         {/* <Route exact path="/" render={props => (
//             <h1>Hello world!</h1>
//         )} /> */}
//     </Router>
// );

//! ====================== Experiment 2 - Dedicated PageService to handle "sub-views" and page transitions/overlays

import PageService from "~/services/PageService";

// TODO: Would it be possible to precompile these definitions to reduce initial computation time?
//>      Maybe a Babel plugin to compute and transpile these calls?
PageService.addNavigationTemplate({
    uid: "library",
    component: () => import("./library/navigation"),
    tags: [ "library" ]
});

PageService.addRedirect("/", "/library");
PageService.addPage({
    uid: "library", paths: "/library",
    component: () => import("./library"),
    navigationTemplate: "library"
});

export default PageService.generateRouting();

//! ====================== Experiment 3 - PageRenderer. Basically a different approach to Experiment 2

// import PageRenderer from "~/services/PageRenderer";
// export default PageRenderer.buildPageRenderer();
