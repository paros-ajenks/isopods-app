import React from "react";
import classNames from "~/util/classnames";
import throttle from "lodash/throttle";

const measureText = (text, size) => {
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    ctx.font = `${size} "Muli", sans-serif`;
    
    return ctx.measureText(text).width;
};

export default class TextLine extends React.PureComponent {
    state = { marquee: false, speed: 0 };

    componentDidMount() {
        window.addEventListener("resize", this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize = throttle(() => {
        if(this.text && this.container) {
            let cw = this.container.clientWidth;
            let tw = this.text.clientWidth;
            
            this.setState({ marquee: tw - cw > 0, speed: (tw - cw) / 11 });
        }
    }, 200);

    render() {
        let { marquee, speed } = this.state;
        let { children, label = children, style = {}, size, ...props } = this.props;

        let width = measureText(label, size);

        let style1 = {
            ...style,
            width,
            fontSize: size,
            animationDuration: speed.toFixed(2) + "s"
        };

        let style2 = {
            ...style,
            width,
            fontSize: size,
            animationDuration: speed.toFixed(2) + "s"
            // animationDelay: (speed/2).toFixed(2) + "s"
        };
        
        return (
            <div className={classNames("is-text", marquee && "is-marquee")}>
                <div className="inner" ref={ref => this.container = ref}>
                    <h2 {...props} ref={ref => this.text = ref} children={label} style={style1} />
                    {marquee && (
                        <h2 {...props} children={label} className={classNames("marquee-second", props.className)} style={style2} />
                    )}
                </div>
            </div>
        );
    }
}