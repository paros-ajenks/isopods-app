export const rgba = (r,g,b,a=1) => `rgba(${r},${g},${b},${a})`;

const d = 25;

const diff = col => col > 127 ? col - d : col + d;

export const vinylGradient = (r, g, b) => {
    let strip = rgba(r,g,b);
    let back = rgba(diff(r), diff(g), diff(b));

    return `repeating-radial-gradient(${back}, ${back} 6px, ${strip} 7px, ${back} 8px)`;
};

export const vinylStyle = (r, g, b) => ({
    background: vinylGradient(r, g, b),
    borderColor: `rgb(${r}, ${g}, ${b})`
});

export default {
    rgba, vinylGradient, vinylStyle
};
