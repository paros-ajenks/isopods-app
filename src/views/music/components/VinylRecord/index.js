import React, { useMemo } from "react";
import classNames from "~/util/classnames";
import { url } from "~/util/URLParser";
import { vinylStyle } from "./utils";

import "./style.scss";

export default ({
    color = [30, 30, 30],
    isRecord,
    playing,
    sticker,
    onClick
}) => {
    const styles = useMemo(() => vinylStyle(...color), [color]);
    
    return (
        <div className={classNames("vinyl vinyl-circle", isRecord && "is-circle", playing && "is-spinning")} style={styles} onClick={onClick}>
            <div className="vinyl-circle vinyl-sticker" style={{ backgroundImage: url(sticker) }} />
        </div>
    );
};
