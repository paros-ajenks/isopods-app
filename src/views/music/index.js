import React from "react";
import classNames from "~/util/classnames";

import Primitive from "~/components/neuomorphic/Primitive";
// import DigitalClock from "~/components/neuomorphic/DigitalClock";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";
// import Toggle from "~/components/neuomorphic/Toggle";
// import Card from "~/components/neuomorphic/Card";
// import Icon from "~/components/neuomorphic/Icon";
import Text from "~/components/neuomorphic/Text";
// import Col from "~/components/neuomorphic/Col";
import Row from "~/components/neuomorphic/Row";

import AlbumCover from "~/components/AlbumCover";

import VinylRecord from "./components/VinylRecord";
import TextLine from "./components/TextLine";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

const tryRead = async() => {
    let handle = await window.showDirectoryPicker();
    for await (const entry of handle.values()) {
        console.log(entry);
    }
};

export default () => {
    const [ playing, setPlaying ] = React.useState(true);
    const [ vinyl, setVinyl ] = React.useState(false);
    const color = [36, 85, 96];

    const sticker = "https://img.discogs.com/RMUpvywIa_9eZpZ2jpmjxKuDiEI=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3800429-1438524668-5348.jpeg.jpg";

    return (
        <React.Fragment>
            <Navbar className="is-flex has-aligned-center has-justified-between is-z-1">
                <Text as="h5" bold className="has-m-0 is-inline">Now Playing</Text>
                <Button variant="icon-square" icon="queue_music" className="has-m-0 is-inline-flex" onClick={tryRead} />
            </Navbar>
            
            <Row className="has-justified-center has-aligned-start is-relative">
                <Primitive shape={vinyl ? "circle" : "square"} className="vinyl-container">
                    <AlbumCover visible={!vinyl} src={sticker} />
                    <VinylRecord isRecord={vinyl} playing={playing} color={color} sticker={sticker} onClick={() => setVinyl(!vinyl)} />
                </Primitive>

                <div className="controls">
                    <Button variant="icon-square" size="sm" icon="repeat" className={classNames("has-m-0 is-primary-box is-inline-flex", !vinyl && "has-m-3 is-flat")} />
                    <Button variant="icon-square" size="sm" icon="shuffle" className={classNames("has-m-0 is-primary-box is-inline-flex", !vinyl && "has-m-3 is-flat")} />
                </div>
            </Row>

            <Row className="song-info has-mt-3">
                <TextLine size="1.75rem" className="song-title">Ship of Gold</TextLine>
                <TextLine size="1.2rem" className="artist">Clutch</TextLine>
                <TextLine size="1rem" className="album-title">The Elephant Riders</TextLine>
            </Row>

            <Row className="media-controls">
                <div className="section is-main">
                    <Button variant="icon-square" size="lg" icon="skip_previous" className="has-m-0 is-inline-flex" />
                    <Button variant="icon-square" size="xl" icon={playing ? "pause" : "play_arrow"} className="play-button" onClick={() => setPlaying(!playing)} />
                    <Button variant="icon-square" size="lg" icon="skip_next" className="has-m-0 is-inline-flex" />
                </div>
            </Row>
        </React.Fragment>
    );
};
