import React from "react";
import classNames from "~/util/classnames";
import { switcher } from "~/util/circuitry";
import { getScrollParent } from "~/util/dom";
import Contexts from "~/hooks/useContext";

import SwipeableViews from "react-swipeable-views";

import PlaylistsSection from "./sections/playlists";
import ArtistsSection from "./sections/artists";
import AlbumsSection from "./sections/albums";
import SongsSection from "./sections/songs";

import tabs from "./tabs";

import "~/components/neuomorphic/base.scss";
import "./style.scss";

const tabComponents = {
    "playlists": PlaylistsSection,
    "artists": ArtistsSection,
    "albums": AlbumsSection,
    "songs": SongsSection
};

let views = tabs.map(x => ({ ...x, Component: tabComponents[x.id] }));

const InnerComponent = ({
    onSelectTabIndex,
    onSwitching,
    viewRefs,
    scrolled
}) => {
    let selectedTab = Contexts.useContext("libraryTab");
    let selectedTabIndex = React.useMemo(() => views.findIndex(x => x.id === selectedTab), [ selectedTab ]);

    console.log("rerender");

    return (
        <SwipeableViews className="swipe-container" index={selectedTabIndex} onChangeIndex={onSelectTabIndex} onSwitching={onSwitching} axis="x">
            {views.map(({ Component, style, ...tab }) => (
                <section key={`tabbody-${tab.id}`}
                    className={classNames("library-section", `is-tab-${tab.id}`, tab.id === selectedTab && "is-visible")}
                    style={{ ...style, top: scrolled[tab.id] }}
                    ref={ref => viewRefs[tab.id] = ref}>
                    <Component tab={tab} />
                </section>
            ))}
        </SwipeableViews>
    );
};

export default class HomeView extends React.PureComponent {
    state = {
        scrolled: views.reduce((o,k) => ({...o, [k]: 0}), {}),
        selectedTab: views[0].id,
        selectedTabIndex: 0
    };

    viewRefs = views.reduce((o,k) => ({...o, [k]: null}), {});

    onSelectTab = selectedTab => this.setState({
        selectedTabIndex: views.findIndex(x => x.id === selectedTab),
        selectedTab
    });

    onSelectTabIndex = selectedTabIndex => this.setState({
        selectedTabIndex,
        selectedTab: views[selectedTabIndex].id
    });

    onStartSwiping = () => {
        let { selectedTab, scrolled } = this.state;
        console.log("START SWIPE");
        let pos = getScrollParent(this.viewRefs[selectedTab]);
        pos = pos ? pos.scrollTop : 0;
        document.body.classList.add("is-transitioning");

        this.setState({
            previousTab: selectedTab,
            scrolled: {
                ...scrolled,
                [selectedTab]: pos
            }
        });
    };

    onStopSwiping = index => {
        // let previousTab = this.state.selectedTab;
        console.log("STOP SWIPE");
        let update = { ...this.state.scrolled };
        let tab = views[index];
        document.body.classList.remove("is-transitioning");
        document.body.setAttribute("data-mobile-view", tab.id);

        update[tab.id] = 0;

        this.setState({ scrolled: update, previousTab: null }, () => {
            Contexts.setStateValue("libraryTab", tab.id);
            // console.log("POST-UPDATE RAN!", { update, previousTab });
        });
    };

    onSwitching = switcher(1, (i, type) => {
        console.log("SWITCH");
        if(type === "move") {
            // console.log("Starting swipe");
            return this.onStartSwiping();
        } else if(type === "end") {
            // console.log(`Stopping swipe ${this.state.selectedTab} -> ${i}`);
            return this.onStopSwiping(i);
        }
    });

    render() {
        let { scrolled } = this.state;

        return (
            <InnerComponent key="view" scrolled={scrolled} onSwitching={this.onSwitching} onSelectTabIndex={this.onSelectTabIndex} viewRefs={this.viewRefs} />
        );

        // return (
        //     <React.Fragment>
        //         <SwipeableViews className="swipe-container" index={selectedTabIndex} onChangeIndex={this.onSelectTabIndex} onSwitching={this.onSwitching}>
        //             {views.map(({ Component, style, ...tab }) => (
        //                 <section key={`tabbody-${tab.id}`}
        //                     className={classNames("library-section", tab.id === selectedTab && "is-visible")}
        //                     style={{ ...style, top: scrolled[tab.id] }}
        //                     ref={ref => this.viewRefs[tab.id] = ref}>
        //                     <Component tab={tab} />
        //                 </section>
        //             ))}
        //         </SwipeableViews>
        //     </React.Fragment>
        // );
    }
}
