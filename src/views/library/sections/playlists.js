import React from "react";
// import { nanoid } from "nanoid";
// import fuzzySearch from "fuzzysearch";
import Toggles from "~/hooks/useToggle";
import useStickyWatcher from "~/hooks/useStickyWatcher";

// import AlbumDisplayList from "~/components/AlbumDisplayList";
import ActionSubBar from "~/components/navigation/ActionsSubBar";
import SearchButton from "~/components/form/SearchButton";
import Button from "~/components/neuomorphic/Button";

// let demo = Array(5).fill(0).map((x, i) => {
//     let id = nanoid(12);

//     return {
//         id, title: i % 2 ? `Mixtape ${id}` : `Playlist ${id}`
//     };
// });

export default () => {
    let [ /*filter*/, setFilter ] = React.useState(null);
    let [ displayMode, toggleDisplayMode ] = Toggles.usePersistentToggle("ui:library:playlist:display", "grid", "list");
    let [ stickyRef ] = useStickyWatcher();

    // let list = React.useMemo(() => {
    //     if(!filter) return demo;
    //     return demo.filter(playlist => fuzzySearch(filter, playlist.title.toLowerCase()));
    // }, [ filter ]);

    let gridIcon = displayMode === "grid" ? "view_module" : "view_stream";

    const onSearch = React.useCallback(text => setFilter((text||"").toLowerCase()), [ setFilter ]);

    return (
        <React.Fragment>
            <ActionSubBar ref={stickyRef}>
                <Button variant="icon" iconVariant="outline" className="is-flat has-m-0" icon={gridIcon} size="md" onClick={toggleDisplayMode} />
                <SearchButton placeholder="Find a mixtape..." onSearch={onSearch} />
            </ActionSubBar>

            {/* <AlbumDisplayList albums={list} displayMode={displayMode} /> */}
        </React.Fragment>
    );
};
