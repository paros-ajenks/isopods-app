import React from "react";
import useStickyWatcher from "~/hooks/useStickyWatcher";

import ActionSubBar from "~/components/navigation/ActionsSubBar";
// import AlbumDisplayList from "~/components/AlbumDisplayList";
import Button from "~/components/neuomorphic/Button";

// let demo = [
//     { id:  1,  title: "In Miracle Land", subtitle: "The Vines", art: "https://images-na.ssl-images-amazon.com/images/I/81BOalbQ3PL._SL1500_.jpg" },
//     { id:  2,  title: "Gravity X", subtitle: "Truckfighters", art: "https://upload.wikimedia.org/wikipedia/en/f/f2/Gravity_X_album_cover.png" },
//     { id:  3,  title: "Strange Cousins From the West", subtitle: "Clutch", art: "https://img.discogs.com/cfy0vmdRUk1apgG7WjfWLAjj2fI=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-5046904-1431369118-8782.jpeg.jpg" },
//     { id:  4,  title: "Vessel", subtitle: "Lorn", art: "https://f4.bcbits.com/img/a0665612729_10.jpg" },
//     { id:  5,  title: "OK Computer", subtitle: "Radiohead", art: "https://img.discogs.com/yIM4lSU_7uAcYRUP_paNthymO30=/fit-in/600x593/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1399301-1404414270-1807.jpeg.jpg" },
//     { id:  6,  title: "Them Crooked Vultures", subtitle: "Them Crooked Vultures", art: "https://upload.wikimedia.org/wikipedia/en/3/33/ThemCrookedVulturesCover.jpg" },
//     { id:  7,  title: "Hawaii: Part II", subtitle: "ミラクルミュージカル", art: "https://i1.sndcdn.com/artworks-000602170645-fe2its-t500x500.jpg" },
//     { id:  8,  title: "Transmalinnia", subtitle: "Lumerians", art: "https://images-na.ssl-images-amazon.com/images/I/61TR2mAIzkL._SX466_.jpg" },
//     { id:  9,  title: "Magnified", subtitle: "Failure", art: "https://upload.wikimedia.org/wikipedia/en/8/88/Failure-Magnified.jpg" },
//     { id:  10, title: "All Natural", subtitle: "Kupla", art: "https://f4.bcbits.com/img/a3415635694_10.jpg" },
//     { id:  11, title: "III", subtitle: "Föllakzoid", art: "https://f4.bcbits.com/img/a3072133502_10.jpg" },
//     { id:  12, title: "Ourdom", subtitle: "Solar Fields", art: "https://img.discogs.com/mE3VhborFMA3FIJ-X4GdBHuJhYE=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-11784579-1522332886-9119.jpeg.jpg" }
// ];

export default () => {
    let [ stickyRef ] = useStickyWatcher();

    return (
        <React.Fragment>
            <ActionSubBar ref={stickyRef} className="has-justified-end">
                <Button variant="icon" iconVariant="outline" className="is-flat has-m-0" icon="search" size="md" />
            </ActionSubBar>

            {/* <AlbumDisplayList albums={demo} /> */}
        </React.Fragment>
    );
};
