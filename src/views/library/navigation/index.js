import React from "react";

import Contexts from "~/hooks/useContext";

import TopTabNavigation from "~/components/neuomorphic/TopTabNavigation";
import CurrentlyPlayingBar from "~/components/CurrentlyPlayingBar";
import Button from "~/components/neuomorphic/Button";
import Navbar from "~/components/neuomorphic/Navbar";

import tabs from "../tabs";

import "~/components/neuomorphic/base.scss";

const Tabs = () => {
    let [ selectedTab, setSelectedTab ] = Contexts.useState("libraryTab", "playlists");

    return (
        <TopTabNavigation className="is-raised" tabs={tabs} selectedTab={selectedTab} onSelectTab={tab => setSelectedTab(tab.id)} />
    );
};

let album = {
    id: "asdf",
    title: "No One Loves Me and Neither Do I",
    subtitle: "Them Crooked Vultures",
    art: "https://upload.wikimedia.org/wikipedia/en/3/33/ThemCrookedVulturesCover.jpg"
};

export default ({ children }) => {
    return (
        <React.Fragment>
            <Navbar className="is-flex has-aligned-center has-justified-between" variant="sticky">
                <div>
                    <Button variant="icon-square" icon="menu" className="has-m-0 has-mr-4 is-inline-flex" />
                    <span className="nav-title">My Library</span>
                </div>
            </Navbar>

            <Tabs />

            {children}

            <CurrentlyPlayingBar album={album} playing />
        </React.Fragment>
    );
};
