export const tabs = [
    { id: "playlists", label: "Mixtapes" },
    { id: "artists",   label: "Artists" },
    { id: "albums",    label: "Albums" },
    { id: "songs",     label: "Songs" }
];

export default tabs;