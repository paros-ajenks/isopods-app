import React = require("react");

declare module "react" {
    function forwardRef<T, P = {}>(render: T): T;

    type ContextProvider<P = {}> = React.FunctionComponent<P>;
}
