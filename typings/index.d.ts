require("./react");

declare type LazyComponentFactory<T> = Promise<{ default: React.ComponentType<T> }>;

declare type DeviceTypes = "default" | "mobile" | "desktop" | "android" | "ios" | "electron";

declare type DeviceTypeFactory<T> = {
    default?: T;
    android?: T;
    desktop?: T;
    electron?: T;
    ios?: T;
    mobile?: T;
}
